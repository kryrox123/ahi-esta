import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { filter, map,take } from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  url: string = 'https://randomuser.me/api/';

  constructor(public http: HttpClient) { }

    obtenerUsuario(): Observable<any>{
      return this.http.get<any>(this.url);
    }

    obtenerUsuarioMasculino(): Observable<any>{
      return this.http.get<any>(this.url).
              pipe(
                filter(
                  usuario => usuario['results'][0].gender != 'male'
                )
              );
    }
    obtenerPais(): Observable<any>{
      return this.http.get<any>(this.url).
      pipe(
        filter(
          pais => pais['results'][0].location.country != "Canada"
          ) 
      )
    }
    obtenerCantidadElementos(): Observable<any>{
      return this.http.get<any>(this.url).
              pipe(
                take(0)
              );
    }

    obtenerFoto(): Observable<any>{
      return this.http.get<any>(this.url).
              pipe(
                map(resp => {
                  console.log(resp);
                  return resp['results'].map((usuario: any) => {
                    console.log(usuario);
                    return {
                      name: usuario.name,
                      picture: usuario.picture
                    }
                  });
                })
              );
    }  
    // obtenerFotos3(): Observable<any>{
    //   return this
    // }
  
  }
