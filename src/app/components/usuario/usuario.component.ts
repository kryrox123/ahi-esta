import { Component, OnInit } from '@angular/core';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.component.html',
  styleUrls: ['./usuario.component.css']
})
export class UsuarioComponent implements OnInit {

  usuario: any;
  usuario2: any;
  usuario3: any;
  femenino: boolean;
  pais: any;


  constructor(private servcioUsuario: UsuarioService) {
    this.femenino =false;
  }

  ngOnInit(): void {
    this.servcioUsuario.obtenerUsuario().subscribe({
      next: user => {
        //console.log(usuario);
        this.usuario = user['results'][0];
      },
      error: error => {
        console.log(error);
      },
      complete: () => {
        console.log('solicitud completa');
      }
    });
  }

  showFemale():void{
    this.servcioUsuario.obtenerUsuarioMasculino().subscribe({
      next: user => {
        console.log(user);
        this.usuario2 = user['results'][0];
        this.femenino = true;
      },
      error: error => {
        console.log(error);
      }
    });
  }

  Pais(): void {
    this.servcioUsuario.obtenerPais().subscribe({
      next: user => {
        console.log(user);
        this.usuario = user["results"][0];
        this.pais = true;
      },
      error: error => {
        console.log(error);
      }
    });
  }

  MostrarElementos(): void {
    this.servcioUsuario.obtenerCantidadElementos().subscribe({
      next: user => {
        console.log(user);
      },
      error: error => {
        console.log(error);
      }
    });
  }

  MostrarFoto():void{
    this.servcioUsuario.obtenerFoto().subscribe({
      next: user => {
      console.log(user);
      this.usuario3 = user[0];
      },
      error: error => {
        console.log(error);
      }
    });
  }
}
